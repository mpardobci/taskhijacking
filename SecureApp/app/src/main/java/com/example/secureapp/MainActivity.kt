package com.example.secureapp

import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.secureapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupListeners()
    }

    private fun setupListeners(){
        binding.apply {
            btnLogin.setOnClickListener {
                login(emailAddressInput.getStrText(), passwordInput.getStrText())
            }
        }
    }

    private fun login(email: String, password: String){
        //TODO - add login use case
        val intent = Intent(this, LoggedInActivity::class.java)
        startActivity(intent)
    }
}

fun EditText.getStrText(): String {
    return this.text.toString()
}